#Server BattleShips created by MushiX CSys
require 'socket'
require_relative 'functions'

puts "Welcome to termShipsOnline!"

server = TCPServer.open(8889)
loop{
  client = server.accept
  puts "You have now opponent to play!"

  map = Array.new(2){Array.new(10){Array.new(10, " ")}}
  score = [0,0]

  drawMaps(map,score)
  map = creatingNavy(map, score)

  while score[0]<20 and score[1]<20
    system "clear"
    drawMaps(map,score)
    # Wait for opponent
		strike(map, score, client.gets, client)
		shoot(map, score, client)
  end
  if(score[0]==20)
    print "You win! Congratulations"
  else
    print "Defeat"
  end

  client.close
}