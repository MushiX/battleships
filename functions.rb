def drawMaps(map, score)
	puts "     A B C D E F G H I J      A B C D E F G H I J  "
	puts "   + - - - - - - - - - - +  + - - - - - - - - - - +"
	10.times do |y|
		print (y+1).to_s+"  | " if y<9
		print (y+1).to_s+" | " if y==9
		10.times do |x|
			print map[0][x][y]+" "
		end
		print "|  | "
		10.times do |x|
			print map[1][x][y]+" "
		end
		puts "| "+(y+1).to_s
	end
	puts "   + - - - - - - - - - - +  + - - - - - - - - - - +"
	puts "     A B C D E F G H I J      A B C D E F G H I J  \n"
	puts "        Your Score: "+score[0].to_s+"          Opponent Score: "+score[1].to_s
end

def getCoordinates(*args)
	if args.length == 3
		tail = STDIN.gets
		x = tail.slice(0, tail.index(" ")).ord - 65
		y = tail[tail.index(" ")+1..tail.rindex(" ")-1].to_i - 1
		tail = tail.slice(tail.rindex(" ")+1)
		return x, y, tail
	else
		y = STDIN.gets
		x = y.slice(0, y.index(" ")).ord - 65
		y = y[y.rindex(" ")+1..-1].to_i - 1
		return x, y
	end
end

=begin
def getCoordinates(x, y)
	y = STDIN.gets
	x = y.slice(0, tail.index(" ")).ord - 65
	y = y.slice(y.rindex(" ")+1)
	return x, y
end
=end

def canCreateShip(map, x, y, tail, flagpole)
	flagpole-=1
	case tail
		when 't'
			for i in y-flagpole..y
				return false if map[0][x][i]=='O'
			end
		when 'b'
			for i in y..y+flagpole
				return false if	map[0][x][i]=='O'
			end
		when 'r'
			for i in x..x+flagpole
				return false if map[0][i][y]=='O'
			end
		when 'l'
			for i in x-flagpole..x
				return false if map[0][i][y]=='O'
			end
	end
	return true
end

def createShip(map, x, y, tail, flagpole)
	flagpole-=1
	case tail
		when 't'
			for i in y-flagpole..y
				map[0][x][i]='O'
			end
		when 'b'
			for i in y..y+flagpole
				map[0][x][i]='O'
			end
		when 'r'
			for i in x..x+flagpole
				map[0][i][y]='O'
			end
		when 'l'
			for i in x-flagpole..x
				map[0][i][y]='O'
			end
	end
end

def creatingNavy(map, score)
	x, y, tail = 0, flagpole = 4;

	puts "Use x and y coordination and position last ship t-top, r-right, b-bottom or l-left like that: B 4 b\n"
	for i in 1..4
		i.times do |j|
			begin
				print "Where want own #{flagpole} mastbase: "
				x, y, tail = getCoordinates(x, y, tail)
				redo if (x<0 and y<0 and x>9 and y>9) or (y>9-(flagpole-1) and tail=="b") or (y<flagpole-1 and tail=="t") or (x>9-(flagpole-1) and tail=="r") or (x<flagpole-1 and tail=="l") or (tail!="t" and tail!="r" and tail!="b" and tail!="l")
			end while !canCreateShip(map, x, y, tail, flagpole)
			createShip(map, x, y, tail, flagpole)
			system "clear"
			drawMaps(map, score)
		end
		flagpole-=1
	end

	return map
end

def shoot(map, score, opponent)
	begin
		print "Write coordinate to attack!: "
		x, y = getCoordinates(x, y)
		puts x.to_s+":"+y.to_s
	end while x.to_i<0 or x.to_i>9 or y.to_i>9 or y.to_i<0
	opponent.puts(x.to_s+" "+y.to_s)
	hit = opponent.gets
	if hit.to_i == 1
		score[0]+=1
		map[1][x][y] = 'X'
	else
		map[1][x][y] = '.'
	end
end

def strike(map, score, shooting, opponent)
	shoot = shooting.to_s
	x = shoot.slice(0, shoot.index(" ")).to_i
	y = shoot[shoot.index(" ")+1..shoot.length-1].to_i
	if map[0][x][y] == 'O'
		score[1]+=1
		opponent.puts(1)
		map[0][x][y] = 'X'
	else
		opponent.puts(0)
		map[0][x][y] = '.'
	end
end