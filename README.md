# battleShips [Ruby Console Game]
This is simple game with socket written in Ruby. Enjoy to play, and funny to edit code(only for begginers)! ;)

## Instalation
Arch/KaOS/Manjaro/Antergos:  
$ sudo pacman -S ruby

OpenSUSE/SLED/SLES:  
$ sudo zypper install ruby

Debian/*buntu/Mint:  
$ sudo apt install ruby

Gentoo/Sabayon with Portage:  
$ sudo emerge ruby

Sabayon with Entropy:  
$ sudo equo ruby

MacOS with Homebrew:  
-> hmm... MacOS have ruby out of the box ?;)

## Usage
You must run first server file:  
$ ruby server_ships.rb

After this time, your opponent must run client file:  
$ ruby client_ships.rb

## Status Project
This is a earlier final version. This is only scaffold project. You can searching a bugs and writting me message for your opinion at my code, because this is my firstly project with usage sockets in Ruby.